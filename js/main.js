/*
 Masked Input plugin for jQuery
 Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
 Version: 1.3.1
 */
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);


$(document).ready(function(){

    // слайдер
    $('.slider-block').owlCarousel({
        margin:0,
        loop: true,
        autoWidth:false,
        nav: true,
        dots: true,
        autoplay: true,
        navText: [ , ],
        autoplayHoverPause: true,
        items: 1,
        onInitialized : function(event){
            setTimeout(function(){
                var items = $(".owl-dot").length;
                var item = $('.owl-dot.active').index();
                $('.slider-counter').text(item + 1 + " / " + items);
            }, 100)
        },
        onTranslate : function(event){
            setTimeout(function(){
                var items = $(".owl-dot").length;
                var item = $('.owl-dot.active').index();
                $('.slider-counter').text(item + 1 + " / " + items);
            }, 100)
        }
    });

    $('.tab-slider').each(function(){
        $(this).owlCarousel({
            margin: 30,
            loop: true,
            autoWidth:false,
            nav: true,
            dots: false,
            autoplay: true,
            navText: [ , ],
            items: 4
        })
    });

   // слайд страницы
    $(document).on('click', 'a.js--page-slide', function(){
        $('.main-nav li').removeClass('active')
        $(this).parents().addClass('active')
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top - 42 + "px"
        }, {
            duration: 500
        });
        return false;
    });

    var submenu = $('.sub');
    var navigation_links = $('.main-nav a');

    submenu.waypoint(function(direction) {
        var active_section = $(this);
        if (direction == "up") active_section = active_section.prev();
        var active_link = $('.main-nav a[href="#' + active_section.data("link") + '"]');
        navigation_links.removeClass("active");
        active_link.addClass("active")
    },{offset: '47px'});

    //форма
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

     $('.input-phone').mask('+7(999)999-99-99');

    // фокус поля
    $(document).on('focus', '.form-input', function(){
        $(this).removeClass('error');
    });

    // отправка формы
    $(document).on('click', '.js-form-submit', function () {

        var form =  $(this).parents('.main-form');
        var errors = false;

        $(form).find('.required').each(function(){
            var val=$(this).prop('value');
            if(val==''){
                $(this).addClass('error');
                errors=true;
            }
            else{
                if($(this).hasClass('input-mail')){
                    if(validateEmail(val) == false){
                        $(this).addClass('error');
                        errors=true;
                    }
                }
            }
        });

        if(errors == false){
            var button_value = $(form).find('.js-form-submit').text();
            $(form).find('.js-form-submit').text('Wait...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();
            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function(data) {
                    form.find('.form-input').each(function(){
                        $(this).prop('value','')
                    });
                    $(form).find('.js-form-submit').text(button_value);
                    $('.js--form-ok').trigger('click')
                },
                error: function(data) {
                    $(form).find('.js-form-submit').text('Error');
                    setTimeout(function() {
                        $(form).find('.js-form-submit').text(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

    // табы
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
      });

    // яндекс-карта
    if ($('#map').length) {
       var myMap;
        function init() {
            myMap = new ymaps.Map('map', {
                center: [56.486236,84.949915],
                zoom: 14,
                controls: ['zoomControl', 'searchControl']
            });
            myMap.controls.add('zoomControl', { left: 5, top: 5 });
            myPlacemark = new ymaps.Placemark([56.481700,84.950778], {}, {
                iconLayout: 'default#image',
                iconImageHref: 'img/icon--map.png',
                iconImageSize: [87, 89],
                iconImageOffset: [-82, -33]
            });
            myPlacemark1 = new ymaps.Placemark([56.493836,84.948756], {}, {
                iconLayout: 'default#image',
                iconImageHref: 'img/icon--map.png',
                iconImageSize: [87, 89],
                iconImageOffset: [-42, -103]
            });
            myMap.geoObjects.add(myPlacemark);
            myMap.geoObjects.add(myPlacemark1);
            myMap.behaviors.disable('scrollZoom');
        }
        ymaps.ready(init);
    }

    $('.fancy').fancybox();

    // попапы

    $(document).on('click', '.js--change-act', function(){
        $('.act-form').find('.content h2').html( $(this).attr('data-title') + "<strong>" + $(this).attr('data-subtitle') +"</strong>");
        $('.act-form').find('.content .img').attr('src', $(this).attr('data-img'));
        $('.act-form').find('.main-form .hidden').prop('value', $(this).attr('data-form'));
    })

    $(document).on('click', '.js--change-item', function(){
        var block = $(this);
        if($(this).hasClass('change-set')){
            $('.item-form').find('.content .name').text( block.attr('data-name'));
            $('.item-form').find('.content .price').text( block.attr('data-price'));
            $('.item-form').find('.content .old').text( block.attr('data-old'));
            $('.item-form').find('.content ul').html( block.attr('data-list'));
            $('.item-form').find('.content .img').attr('src', block.attr('data-img'));
            $('.item-form').find('.main-form .hidden').prop('value', block.attr('data-form'));
        }else{
            $('.item-form').find('.content .name').text(block.attr('data-name'));
            $('.item-form').find('.content .price').text( block.attr('data-price'));
            $('.item-form').find('.content .img').attr('src', block.attr('data-img'));
            $('.item-form').find('.main-form .hidden').prop('value', block.attr('data-form'));
        }
    })

})
